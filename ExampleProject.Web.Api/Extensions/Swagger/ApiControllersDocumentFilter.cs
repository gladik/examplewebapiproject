﻿using System.Collections.Generic;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace ExampleProject.Web.Api.Extensions.Swagger
{
    public class ApiControllersDocumentFilter: IDocumentFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            var paths = new Dictionary<string, PathItem>(swaggerDoc.Paths);
            swaggerDoc.Paths.Clear();
            foreach (var path in paths)
            {
                if (path.Key.Contains("api/"))
                    swaggerDoc.Paths.Add(path);
            }
        }
    }
}
