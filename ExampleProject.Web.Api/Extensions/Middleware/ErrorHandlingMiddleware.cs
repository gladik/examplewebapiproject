﻿using System;
using System.Net;
using System.Threading.Tasks;
using ExampleProject.Domain.Model.Entites.Common;
using ExampleProject.Domain.Model.Exceptions;
using ExampleProject.Web.Api.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ExampleProject.Web.Api.Extensions.Middleware
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ErrorHandlingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger<ErrorHandlingMiddleware>();
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                if (!(ex is BusinessException))
                    _logger.LogError(ex, ex.Message);

                await HandleExceptionAsync(context, ex);
            }
        }

        protected virtual Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var response = new ApiResponse(OperationStatus.Error, exception.Message);

            var businessException = exception as BusinessException;
            if (businessException != null)
            {
                response.Status = businessException.Result.Status;
                response.Description = response.Description.Contains("BusinessException") ? businessException.Result.Description : response.Description;
            }

            var model = JsonConvert.SerializeObject(response, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.OK;

            return context.Response.WriteAsync(model);
        }
    }
}
