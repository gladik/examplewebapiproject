﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Net;
using System.Text;
using AutoMapper;
using ExampleProject.Data.Context;
using ExampleProject.Data.Repositories;
using ExampleProject.Data.Repositories.Abstract;
using ExampleProject.Domain.Model.Entites;
using ExampleProject.Domain.Model.Entites.Auth;
using ExampleProject.Domain.Services.Auth;
using ExampleProject.Web.Api.Extensions.Filters;
using ExampleProject.Web.Api.Extensions.Middleware;
using ExampleProject.Web.Api.Extensions.Swagger;
using ExampleProject.Web.Api.Settings;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Email;
using Swashbuckle.AspNetCore.Swagger;

namespace ExampleProject.Web.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private readonly IConfigurationSection _appSettings;
        private readonly IConfigurationSection _logSettings;
        private readonly IConfigurationSection _jwtSettings;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _appSettings = Configuration.GetSection(nameof(AppSettings));
            _logSettings = Configuration.GetSection(nameof(LogSettings));
            _jwtSettings = Configuration.GetSection(nameof(JwtSettings));
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ApplicationDbContext")));

            services.AddIdentity<User, Role>().AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = _jwtSettings[nameof(JwtSettings.Issuer)],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_jwtSettings[nameof(JwtSettings.Key)])),
                    ClockSkew = TimeSpan.Zero
                };
            });

            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
                config.Filters.Add(new AuthorizeFilter(policy));
                config.Filters.Add(new ValidateModelStateFilter());
            });

            services.AddCors();

            services.AddMvc().AddFluentValidation(options =>
            {
                options.RegisterValidatorsFromAssemblyContaining<LoginModel>();
            });

            var emailConnection = new EmailConnectionInfo
            {
                EmailSubject = _appSettings[nameof(AppSettings.EmailSubject)],
                FromEmail = $"{_appSettings[nameof(AppSettings.EmailFromName)]} <{_appSettings[nameof(AppSettings.EmailFrom)]}>",
                ToEmail = _appSettings[nameof(AppSettings.EmailTo)],
                MailServer = _appSettings[nameof(AppSettings.EmailHost)],
                Port = int.Parse(_appSettings[nameof(AppSettings.EmailPort)]),
                NetworkCredentials = !_appSettings[nameof(AppSettings.EmailHost)].Contains("localhost") ? new NetworkCredential(_appSettings[nameof(AppSettings.EmailUsername)], _appSettings[nameof(AppSettings.EmailPassword)]) : null
            };

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Warning()
                .Enrich.FromLogContext()
                .WriteTo.RollingFile(_logSettings[nameof(LogSettings.FilePath)], retainedFileCountLimit: 7)
                .WriteTo.Email(emailConnection, restrictedToMinimumLevel: LogEventLevel.Warning)
                .CreateLogger();

            services.AddScoped<IDbInitializer, DbInitializer>();
            services.AddTransient<IUserRepository, UserRepository>();

            services.AddTransient<IAuthService, AuthService>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "API", Version = "v1" });
                c.IncludeXmlComments(Path.Combine(PlatformServices.Default.Application.ApplicationBasePath,
                    "ExampleProject.Web.Api.xml"));
                c.DocumentFilter<LowercaseDocumentFilter>();
                c.DocumentFilter<ApiControllersDocumentFilter>();
                c.OperationFilter<SwaggerFileOperationFilter>();
                c.OperationFilter<AuthorizationHeaderParameterOperationFilter>();
                c.DescribeAllEnumsAsStrings();
            });

            services.AddMvc();
            services.AddAutoMapper();
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                dbInitializer.Initialize().Wait();
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMiddleware(typeof(ErrorHandlingMiddleware));

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto,
                ForwardLimit = 3
            });

            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API");
                c.DefaultModelsExpandDepth(-1);
            });
        }
    }
}
