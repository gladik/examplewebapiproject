﻿using System;
using System.Linq;
using ExampleProject.Domain.Model.Entites.Common;
using ExampleProject.Domain.Model.Extensions;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace ExampleProject.Web.Api.Models
{
    public class ApiResponse
    {
        public int Status { get; set; }
        public string Description { get; set; }
        public long Timestamp { get; set; }
        public object Result { get; set; }

        public ApiResponse()
        {
            Status = OperationStatus.Success.Status;
            Timestamp = DateTime.UtcNow.ConvertToTimestamp();
        }

        public ApiResponse(object result)
        {
            Result = result;
            Status = OperationStatus.Success.Status;
            Timestamp = DateTime.UtcNow.ConvertToTimestamp();
        }

        public ApiResponse(object result, int statusCode, string message)
        {
            Result = result;
            Status = statusCode;
            Timestamp = DateTime.UtcNow.ConvertToTimestamp();
            Description = message;
        }

        public ApiResponse(ModelStateDictionary state)
        {
            if (state == null) return;

            Status = OperationStatus.Error.Status;
            Description = state.Values.ToList().FirstOrDefault()?.Errors.FirstOrDefault()?.ErrorMessage;
            Timestamp = DateTime.Now.ConvertToTimestamp();
        }

        public ApiResponse(DomainOperationResult result, string description = null)
        {
            Status = result.Status;
            Description = description ?? result.Description;
            Timestamp = DateTime.UtcNow.ConvertToTimestamp();
        }
    }
}
