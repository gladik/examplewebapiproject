﻿using System.Net;
using ExampleProject.Domain.Model.Entites.Employees;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace ExampleProject.Web.Api.Controllers.Employees
{
    [Route("api/employees")]
    public class EmployeesController : BaseController
    {
        [HttpGet("")]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        public IActionResult GetEmployees(EmployeesFilter filter)
        {
            return ApiResponse(null);
        }
    }
}
