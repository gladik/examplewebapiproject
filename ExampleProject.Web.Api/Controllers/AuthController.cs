﻿using System;
using System.Net;
using ExampleProject.Domain.Model.Entites.Auth;
using ExampleProject.Domain.Model.Entites.Common;
using ExampleProject.Domain.Services.Auth;
using ExampleProject.Web.Api.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace ExampleProject.Web.Api.Controllers
{
    [Route("api/auth")]
    public class AuthController : BaseController
    {
        private readonly IAuthService _authService;
        private readonly IConfigurationSection _jwtOptions;

        public AuthController(IAuthService authService, IConfiguration configuration)
        {
            _authService = authService;
            _jwtOptions = configuration.GetSection(nameof(JwtSettings));
        }

        [AllowAnonymous]
        [HttpPost("login")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(TokenModel))]
        public IActionResult Login([FromBody] LoginModel model)
        {
            var user = _authService.Verify(model).Result;
            if (user == null)
            {
                return ApiResponse(OperationStatus.AccessDenied);
            }

            var token = _authService.GenerateToken(new TokenRequest
            {
                User = user,
                ExpiresIn = Convert.ToDouble(_jwtOptions[nameof(JwtSettings.ExpiresIn)]),
                Issuer = _jwtOptions[nameof(JwtSettings.Issuer)],
                Key = _jwtOptions[nameof(JwtSettings.Key)]
            });

            return ApiResponse(token);
        }
    }
}