﻿using ExampleProject.Domain.Model.Entites.Common;
using ExampleProject.Web.Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ExampleProject.Web.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    public abstract class BaseController : Controller
    {
        protected IActionResult ApiResponse(object data, int statusCode, string message)
        {
            return Json(new ApiResponse(data, statusCode, message));
        }

        protected IActionResult ApiResponse(object data)
        {
            return Json(new ApiResponse(data, 200, ""));
        }

        protected IActionResult ApiResponse(int statusCode)
        {
            return Json(new ApiResponse(null, statusCode, ""));
        }

        protected IActionResult ApiResponse(DomainOperationResult operationStatus)
        {
            return Json(new ApiResponse(operationStatus));
        }
    }
}
