﻿namespace ExampleProject.Web.Api.Settings
{
    public class AppSettings
    {
        public string EmailTo { get; set; }
        public string EmailFrom { get; set; }
        public string EmailFromName { get; set; }
        public string EmailSubject { get; set; }
        public string EmailHost { get; set; }
        public int EmailPort { get; set; }
        public int EmailUsername { get; set; }
        public int EmailPassword { get; set; }
    }
}
