﻿namespace ExampleProject.Web.Api.Settings
{
    public class JwtSettings
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
        public int ExpiresIn { get; set; }
    }
}
