﻿using System.Threading.Tasks;

namespace ExampleProject.Data.Context
{
    public interface IDbInitializer
    {
        Task Initialize();
    }
}
