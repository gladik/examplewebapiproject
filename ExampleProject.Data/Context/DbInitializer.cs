﻿using System;
using System.Threading.Tasks;
using ExampleProject.Domain.Model.Entites;
using Microsoft.AspNetCore.Identity;

namespace ExampleProject.Data.Context
{
	public class DbInitializer : IDbInitializer
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly User _user;
        private readonly Role _role;

        public DbInitializer(ApplicationDbContext applicationDbContext, UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            _applicationDbContext = applicationDbContext;
            _userManager = userManager;
            _roleManager = roleManager;

            _role = new Role {
                Name = "Admin"
            };

            _user = new User {
                UserName = "Admin",
                Email = "admin@exampleproject.com",
                EmailConfirmed = true
            };
        }

        public async Task Initialize()
        {
            try
            {
                await _applicationDbContext.Database.EnsureCreatedAsync();

                if (!await _roleManager.RoleExistsAsync(_role.Name))
                {

                    await _roleManager.CreateAsync(_role);
                }

                if (await _userManager.FindByEmailAsync(_user.Email) == null)
                {
                    var result = await _userManager.CreateAsync(_user, "P@ssword123");
                    if (result.Succeeded)
                    {
                        await _userManager.AddToRoleAsync(_user, _role.Name);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
