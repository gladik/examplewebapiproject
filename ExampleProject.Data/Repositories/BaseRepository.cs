﻿using System;
using System.Collections.Generic;
using System.Text;
using ExampleProject.Data.Context;

namespace ExampleProject.Data.Repositories
{
    public abstract class BaseRepository
    {
        protected ApplicationDbContext DbContext { get; set; }

        protected BaseRepository(ApplicationDbContext context)
        {
            DbContext = context;
        }
    }
}
