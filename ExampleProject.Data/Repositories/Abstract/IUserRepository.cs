﻿using System.Threading.Tasks;
using ExampleProject.Domain.Model.Entites;

namespace ExampleProject.Data.Repositories.Abstract
{
    public interface IUserRepository
    {
        Task<User> Get(string email);
        Task<User> Get(string email, string password);
    }
}
