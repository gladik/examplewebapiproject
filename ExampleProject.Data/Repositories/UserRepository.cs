﻿using System.Threading.Tasks;
using ExampleProject.Data.Repositories.Abstract;
using ExampleProject.Domain.Model.Entites;
using Microsoft.AspNetCore.Identity;

namespace ExampleProject.Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager<User> _userManager;
        private readonly IPasswordHasher<User> _passwordHasher;

        public UserRepository(UserManager<User> userManager, IPasswordHasher<User> passwordHasher)
        {
            _userManager = userManager;
            _passwordHasher = passwordHasher;
        }

        public async Task<User> Get(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task<User> Get(string email, string password)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user != null 
                && _passwordHasher.VerifyHashedPassword(user, user.PasswordHash, password) ==PasswordVerificationResult.Success)
            {
                return user;
            }
            return null;
        }
    }
}
