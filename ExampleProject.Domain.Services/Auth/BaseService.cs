﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;

namespace ExampleProject.Domain.Services.Auth
{
    public  abstract class BaseService
    {
        protected IMapper Mapper { get; set; }

        protected BaseService()
        {

        }

        protected BaseService(IMapper mapper)
        {
            Mapper = mapper;
        }

        protected TResult Map<TResult>(object source)
        {
            var mapped = Mapper.Map<TResult>(source);
            return mapped;
        }

        protected List<TResult> MapList<TResult>(object source)
        {
            var mapped = Mapper.Map<List<TResult>>(source);
            return mapped;
        }
    }
}
