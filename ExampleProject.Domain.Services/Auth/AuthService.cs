﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ExampleProject.Data.Repositories.Abstract;
using ExampleProject.Domain.Model.Entites;
using ExampleProject.Domain.Model.Entites.Auth;
using Microsoft.IdentityModel.Tokens;

namespace ExampleProject.Domain.Services.Auth
{
    public class AuthService : IAuthService
    {
        private readonly IUserRepository _userRepository;

        public AuthService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<User> Verify(LoginModel model)
        {
            return await _userRepository.Get(model.Login, model.Password);
        }

        public TokenModel GenerateToken(TokenRequest request)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, request.User.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(request.Key));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.UtcNow.AddMinutes(request.ExpiresIn);

            var token = new JwtSecurityToken(
                request.Issuer,
                null,
                claims,
                expires: expires,
                signingCredentials: signingCredentials
            );

            return new TokenModel
            {
                ExpiresIn = request.ExpiresIn,
                AccessToken = new JwtSecurityTokenHandler().WriteToken(token),
                RefreshToken = "not_implemented",
                Email = request.User.Email,
                Name = request.User.UserName
            };
        }
    }
}
