﻿using System.Threading.Tasks;
using ExampleProject.Domain.Model.Entites;
using ExampleProject.Domain.Model.Entites.Auth;

namespace ExampleProject.Domain.Services.Auth
{
    public interface IAuthService
    {
        Task<User> Verify(LoginModel model);
        TokenModel GenerateToken(TokenRequest request);
    }
}
