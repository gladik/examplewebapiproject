﻿using System;
using ExampleProject.Domain.Model.Entites.Common;

namespace ExampleProject.Domain.Model.Exceptions
{
    public class BusinessException : Exception
    {
        public DomainOperationResult Result { get; set; }

        public BusinessException(DomainOperationResult result) : base(result.Description)
        {
            Result = result;
        }
    }
}
