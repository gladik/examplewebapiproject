﻿namespace ExampleProject.Domain.Model.Entites.Auth
{
    public class TokenModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public double ExpiresIn { get; set; }
    }
}
