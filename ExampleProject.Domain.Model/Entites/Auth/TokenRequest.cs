﻿namespace ExampleProject.Domain.Model.Entites.Auth
{
    public class TokenRequest
    {
        public User User { get; set; }
        public string Key { get; set; }
        public string Issuer { get; set; }
        public double ExpiresIn { get; set; }
    }
}
