﻿namespace ExampleProject.Domain.Model.Entites.Common
{
    public class BaseFilter
    {
        public string Search { get; set; }
        public string OrderBy { get; set; }
        public bool OrderByDescending { get; set; }
        public int? Skip { get; set; }
        public int? Take { get; set; }
    }
}
