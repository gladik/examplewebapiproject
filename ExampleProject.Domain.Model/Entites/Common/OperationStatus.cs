﻿namespace ExampleProject.Domain.Model.Entites.Common
{
    public static class OperationStatus
    {
        #region Base

        public static DomainOperationResult Success = new DomainOperationResult(0);
        public static DomainOperationResult Error = new DomainOperationResult(-1, "Internal server error");
        public static DomainOperationResult NotFound = new DomainOperationResult(-2, "Not found");
        public static DomainOperationResult AccessDenied = new DomainOperationResult(-3, "Access denied");

        #endregion
    }
}
