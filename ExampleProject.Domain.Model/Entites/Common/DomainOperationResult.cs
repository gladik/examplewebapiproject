﻿namespace ExampleProject.Domain.Model.Entites.Common
{
    public class DomainOperationResult
    {
        public int Status { get; set; }
        public string Description { get; set; }

        public DomainOperationResult(int status, string description = null)
        {
            Status = status;
            Description = description;
        }
    }
}
